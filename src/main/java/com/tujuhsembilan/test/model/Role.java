package com.tujuhsembilan.test.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import com.tujuhsembilan.test.configuration.enums.Screens;
import com.tujuhsembilan.test.model.embed.RoleKey;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "wr_role", schema = "public")
public class Role extends AuditableEntity implements GrantedAuthority {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private RoleKey id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "screen_id", insertable = false, updatable = false)
	private Screen screen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_type_id", insertable = false, updatable = false)
	private UserType userType;

	@Column
	private Boolean status;

	@Override
	public String getAuthority() {
		return Screens.getByName(screen.getScreenName()).getGrants();
	}

}
