/**
 * created by : Ryan Ade Saputra
 * created at : 07-01-2021
 */
package com.tujuhsembilan.test.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(schema = "public")
public class UserType extends AuditableEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long userTypeId;

	@Column(length = 25)
	private String name;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "userType")
	private Set<Role> roles;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "userType")
	private Set<Users> users;

}
