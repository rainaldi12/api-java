package com.tujuhsembilan.test.model.embed;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class GroupTypeKey implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "job_group_id")
  private Long jobGroupId;

  @Column(name = "category_code_id")
  private Long categoryCodeId;

}
