package com.tujuhsembilan.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tujuhsembilan.test.model.UserType;

public interface UserTypeRepository extends JpaRepository<UserType, Long> {
}
