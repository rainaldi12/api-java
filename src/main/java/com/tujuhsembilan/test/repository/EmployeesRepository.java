package com.tujuhsembilan.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tujuhsembilan.test.model.Employees;

public interface EmployeesRepository extends JpaRepository<Employees, Long> {
}
