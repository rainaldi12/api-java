package com.tujuhsembilan.test.security.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponseBody {

  private String accessToken;

  private LocalDateTime issuedAt;
  private LocalDateTime expiredOn;

  private String subject;

  private String refreshToken;

}
