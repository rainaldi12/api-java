package com.tujuhsembilan.test.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    // @formatter:off
    registry
      .addMapping("/**") // allow semua endpoint
        .allowedOriginPatterns("*") // untuk diakses dari manapun
        .allowedMethods("*") // dengan method apapun
        .allowedHeaders("*") // dengan menyertakan method apapun
        .allowCredentials(true) // dan diperbolehkan juga pakai cookies
    ;
    // @formatter:on
  }

}
