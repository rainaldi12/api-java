package com.tujuhsembilan.test.configuration;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.tujuhsembilan.lib.i18n.utility.MessageUtil;
import com.tujuhsembilan.test.configuration.enums.Screens;
import com.tujuhsembilan.test.configuration.filter.AuthTokenFilter;
import com.tujuhsembilan.test.configuration.property.SecurityProp;
import com.tujuhsembilan.test.configuration.service.AppUserDetailsSrvc;
import com.tujuhsembilan.test.model.Screen;
import com.tujuhsembilan.test.repository.ScreenRepository;
import com.tujuhsembilan.test.repository.UsersRepository;

import org.modelmapper.ModelMapper;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class ApplicationConfig {

  private final MessageUtil message;

  private final RSAPublicKey rsaPub;
  private final RSAPrivateKey rsaPrv;

  public ApplicationConfig(MessageUtil message) throws NoSuchAlgorithmException {
    this.message = message;

    // ---

    KeyPairGenerator rsaGen = KeyPairGenerator.getInstance("RSA");
    rsaGen.initialize(2048);

    KeyPair rsaKey = rsaGen.genKeyPair();

    this.rsaPub = (RSAPublicKey) rsaKey.getPublic();
    this.rsaPrv = (RSAPrivateKey) rsaKey.getPrivate();
  }

  @Bean
  @Transactional
  public ApplicationRunner init(ScreenRepository screenRepo) {
    return args -> {
      // Generate screens
      if (screenRepo.count() == 0) {
        screenRepo.deleteAll();
        for (Screens screen : Screens.values()) {
          screenRepo.save(
              Screen.builder()
                  .screenName(screen.getName())
                  .build());
        }
      }
    };
  }

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http, AppUserDetailsSrvc userDetailsService,
      JwtDecoder jwtDecoder) throws Exception {
    // @formatter:off
    return http
      .authorizeRequests()
        .antMatchers("/auth/login/**").permitAll()
        .antMatchers("/screen/**").permitAll()
        .antMatchers("/debug/**").permitAll()
        // .antMatchers("/actuator/health/**").permitAll()
        .anyRequest().authenticated()
        .and()
      .addFilterBefore(authTokenFilter(userDetailsService, jwtDecoder), UsernamePasswordAuthenticationFilter.class)
      .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
      .csrf()
        .disable()
      .httpBasic(Customizer.withDefaults())
    .build();
    // @formatter:on
  }

  @Bean
  public AuthenticationManager authenticationManager(HttpSecurity http, AppUserDetailsSrvc userDetailsService,
      SecurityProp securityProp, PasswordEncoder passwordEncoder) throws Exception {
    // @formatter:off
    return http.getSharedObject(AuthenticationManagerBuilder.class)
      .userDetailsService(userDetailsService)
        .passwordEncoder(passwordEncoder)
        .and()
    .build();
    // @formatter:on
  }

  @Bean
  public FilterRegistrationBean<CorsFilter> corsFilter() {
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

    CorsConfiguration config = new CorsConfiguration();
    config.setAllowCredentials(true);
    config.addAllowedOriginPattern("*"); // @Value: http://localhost:8080
    config.addAllowedHeader("*");
    config.addAllowedMethod("*");
    source.registerCorsConfiguration("/**", config);

    FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
    bean.setOrder(0);

    return bean;
  }

  @Bean
  public AuthTokenFilter authTokenFilter(AppUserDetailsSrvc userDetailsService, JwtDecoder jwtDecoder) {
    return new AuthTokenFilter(userDetailsService, jwtDecoder);
  }

  @Bean
  public SecureRandom secureRandom() {
    return new SecureRandom();
  }

  @Bean
  public PasswordEncoder passwordEncoder(SecurityProp securityProp) {
    return new BCryptPasswordEncoder(securityProp.getPasswordStrength());
  }

  @Bean
  public ModelMapper modelMapper() {
    ModelMapper modelMapper = new ModelMapper();
    modelMapper.getConfiguration().setSkipNullEnabled(true);

    return modelMapper;
  }

  @Bean
  public ObjectMapper objectMapper() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

    objectMapper.registerModule(new JavaTimeModule());
    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    return objectMapper;
  }

  @Bean
  public AppUserDetailsSrvc appUserDetailSrvc(UsersRepository userRepo, SecurityProp securityProp,
      PasswordEncoder passwordEncoder) {
    return new AppUserDetailsSrvc(message, userRepo, securityProp, passwordEncoder);
  }

  @Bean
  public JwtDecoder jwtDecoder() {
    return NimbusJwtDecoder.withPublicKey(rsaPub).build();
  }

  @Bean
  public JwtEncoder jwtEncoder() {
    // @formatter:off
    return new NimbusJwtEncoder(
      new ImmutableJWKSet<>(
        new JWKSet(
          new RSAKey
            .Builder(rsaPub)
              .privateKey(rsaPrv)
            .build()
        )
      )
    );
    // @formatter:on
  }

}
