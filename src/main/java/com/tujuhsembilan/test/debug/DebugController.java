package com.tujuhsembilan.test.debug;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("debug")
public class DebugController {

  @GetMapping
  public Boolean debug() {
    return true;
  }

}
