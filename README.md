# Java Spring Login Sample

## Pre Requisite
- Java 11 / OpenJDK 11
- Apache Maven 3.9.0

## Compile / Build Step
- execute `mvn install` to download all depedency for compile / build package
- execute `mvn package` to compile / package the code
- after build step is clear, you can get a result compile / build at folder `target`
- upload file jar `test-0.0.1-SNAPSHOT.jar` in folder `target` to your server